<?php namespace Smartschool\Api\Controllers;

use Exception;
use Validator;

use Illuminate\Routing\Controller;

use League\Fractal;

use Smartschool\Attendance\Models\Attendance as AttendanceModels;

use Smartschool\User\Models\User as UserModels;

class Attendance extends Controller
{
    public function store()
    {
        $user       = UserModels::whereCode(post('code'))->first();
        $attendance = AttendanceModels::firstOrCreate([
            'academy_id' => 1,
            'student_id' => 2
        ]);
        $attendance->save();

        return response()->json([
            'result'  => true,
            'version' => $user
        ]);
    }
}
