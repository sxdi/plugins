<?php

Route::group([
    'prefix'    => 'v1',
    'namespace' => 'Smartschool\Api\Controllers',

    ], function() {
        /**
         * Android
        */
        Route::group(['prefix' => 'attendance'], function() {
            Route::post('/', 'Attendance@store');
        });
    }
);
