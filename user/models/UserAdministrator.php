<?php namespace Smartschool\User\Models;

use Model;

/**
 * UserAdministrator Model
 */
class UserAdministrator extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_user_user_administrators';

    /**
     * @var string The database table used by the model.
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
