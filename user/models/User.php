<?php namespace Smartschool\User\Models;

use Hash;
use Model;
use Smartschool\Core\Classes\Generator;
use Smartschool\Core\Classes\UserManager;
use Smartschool\Core\Classes\SchoolManager;

/**
 * User Model
 */
class User extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_user_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'administrator' => [
            'Smartschool\User\Models\UserAdministrator',
            'key'      => 'id',
            'otherKey' => 'user_id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function getUser()
    {
        $user   = new UserManager;
        return $user->getUser();
    }

    public function getSchool()
    {
        $user   = $this->getUser();
        $school = new SchoolManager;
        return $school->getDetail($user->id);
    }

    public function beforeCreate()
    {
        $school          = $this->getSchool();
        $generator       = new Generator();

        $this->ssid      = $school->code.$generator->makeNumber();
        $this->pin       = $this->ssid;
        $this->passcode  = Hash::make($this->ssid);
        $this->parameter = $generator->make();
    }
}
