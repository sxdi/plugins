<?php namespace Smartschool\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUserAdministratorsTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_user_user_administrators', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id');
            $table->integer('school_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_user_user_administrators');
    }
}
