<?php namespace Smartschool\Course\Models;

use Model;
use Smartschool\Core\Classes\Generator;

/**
 * Course Model
 */
class Course extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_course_courses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'type' => [
            'Smartschool\Course\Models\Type',
            'key'      => 'type_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
