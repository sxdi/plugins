<?php namespace Smartschool\Course\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTypesTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_course_types', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->string('parameter');
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_course_types');
    }
}
