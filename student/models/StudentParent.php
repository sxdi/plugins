<?php namespace Smartschool\Student\Models;

use Model;

/**
 * StudentParent Model
 */
class StudentParent extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_student_student_parents';

    /**
     * @var string The database table used by the model.
     */
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['student_id', 'type'];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];
}
