<?php namespace Smartschool\Student\Models;

use Model;
use Smartschool\Core\Classes\Generator;
use Smartschool\Core\Classes\UserManager;
use Smartschool\Core\Classes\SchoolManager;


/**
 * Student Model
 */
class Student extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_student_students';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'parents' => [
            'Smartschool\Student\Models\StudentParent',
            'key'      => 'student_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];


    public function getSchool()
    {
        $user   = new UserManager;
        $user   = $user->getUser();

        $school = new SchoolManager;
        return $school->get($user->id);
    }

    public function getAllBySchool()
    {
        $school = $this->getSchool();
        return $this->whereSchoolId($school)->get();
    }

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->school_id = $this->getSchool();
        $this->parameter = $generator->make();
    }
}
