<?php namespace Smartschool\Student\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateStudentParentsTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_student_student_parents', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('student_id');
            $table->enum('type', ['father', 'mother', 'guardian'])->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('education')->nullable();
            $table->string('job')->nullable();
            $table->string('religion')->nullable();
            $table->text('address')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_student_student_parents');
    }
}
