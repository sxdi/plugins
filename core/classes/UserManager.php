<?php namespace Smartschool\Core\Classes;

use Session;

use Smartschool\User\Models\User as UserModels;

class UserManager
{
    public function putSession($user)
    {
        Session::set('userLogin', $user);
    }

    public function getUser()
    {
        $user = Session::get('userLogin');
        $user = UserModels::find($user->id);
        return $user;
    }

    public function destroySession()
    {
        Session::forget('userLogin');
    }
}
