<?php namespace Smartschool\Core\Classes;

use Hash;

class Generator
{
    public function make()
    {
        return md5(Hash::make(date('Y-m-d H:i:s')).rand(0, 99999));
    }

    public function makeNumber()
    {
        return rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
    }

    public function makeSchoolCode()
    {
        return rand(0,9).rand(0,9).rand(0,9);
    }
}
