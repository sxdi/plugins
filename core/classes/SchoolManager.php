<?php namespace Smartschool\Core\Classes;

use Smartschool\Academy\Models\Academy as AcademyModels;

use Smartschool\User\Models\UserAdministrator as UserAdministratorModels;
use Smartschool\School\Models\School as SchoolModels;

class SchoolManager
{
    public function get($userId)
    {
        return UserAdministratorModels::whereUserId($userId)->first()->school_id;
    }

    public function getDetail($userId)
    {
        $school = $this->get($userId);
        return SchoolModels::find($school)->first();
    }

    public function getAcademy($userId)
    {
    	$school = $this->get($userId);
    	return AcademyModels::whereSchoolId($school)->first()->id;
    }
}
