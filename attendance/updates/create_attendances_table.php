<?php namespace Smartschool\Attendance\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAttendancesTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_attendance_attendances', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('academy_id');
            $table->integer('student_id');
            $table->timestamps();
            $table->boolean('is_system')->nullable()->default(0);
            $table->integer('teacher_id')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_attendance_attendances');
    }
}
