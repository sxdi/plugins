<?php namespace Smartschool\Attendance;

use Backend;
use System\Classes\PluginBase;

/**
 * Attendance Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Attendance',
            'description' => 'No description provided yet...',
            'author'      => 'Smartschool',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Smartschool\Attendance\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'smartschool.attendance.some_permission' => [
                'tab' => 'Attendance',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'attendance' => [
                'label'       => 'Attendance',
                'url'         => Backend::url('smartschool/attendance/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['smartschool.attendance.*'],
                'order'       => 500,
            ],
        ];
    }
}
