<?php namespace Smartschool\Classroom\Models;

use Model;
use Smartschool\Core\Classes\Generator;

/**
 * Type Model
 */
class Type extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_classroom_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
