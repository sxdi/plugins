<?php namespace Smartschool\Classroom\Models;

use Model;
use Smartschool\Core\Classes\UserManager;
use Smartschool\Core\Classes\SchoolManager;
use Smartschool\Core\Classes\Generator;

/**
 * Classroom Model
 */
class Classroom extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_classroom_classrooms';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'level' => [
            'Smartschool\Classroom\Models\Level',
            'key'      => 'level_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function getAcademy()
    {
        $user    = new UserManager;
        $user    = $user->getUser();

        $school  = new SchoolManager;
        $academy = $school->getAcademy($user->id);
        return $academy;
    }

    public function getAll()
    {
        return $this->whereAcademyId($this->getAcademy())->get();
    }

    public function beforeCreate()
    {
        $generator        = new Generator();
        $this->academy_id = $this->getAcademy();
        $this->parameter  = $generator->make();
    }

    public function beforeSave()
    {
        $this->slug = str_slug($this->name);
    }
}
