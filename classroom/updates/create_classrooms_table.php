<?php namespace Smartschool\Classroom\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateClassroomsTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_classroom_classrooms', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('academy_id');
            $table->integer('level_id');
            $table->integer('type_id')->nullable();
            $table->string('name');
            $table->timestamps();
            $table->string('slug');
            $table->string('parameter');
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_classroom_classrooms');
    }
}
