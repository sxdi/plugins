<?php namespace Smartschool\Classroom\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateClassroomStudentsTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_classroom_classroom_students', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('classroom_id');
            $table->integer('student_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_classroom_classroom_students');
    }
}
