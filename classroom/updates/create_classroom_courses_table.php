<?php namespace Smartschool\Classroom\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateClassroomCoursesTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_classroom_classroom_courses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('classroom_id');
            $table->string('course_id');
            $table->string('teacher_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_classroom_classroom_courses');
    }
}
