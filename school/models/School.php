<?php namespace Smartschool\School\Models;

use Model;
use Smartschool\Core\Classes\Generator;

/**
 * School Model
 */
class School extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_school_schools';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->code      = $generator->makeSchoolCode();
        $this->parameter = $generator->make();
    }

    public function beforeSave()
    {
        $this->slug = str_slug($this->name);
    }
}
