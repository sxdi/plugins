<?php namespace Smartschool\School\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSchoolsTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_school_schools', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->timestamps();
            $table->string('slug');
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_school_schools');
    }
}
