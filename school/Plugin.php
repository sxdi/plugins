<?php namespace Smartschool\School;

use Backend;
use System\Classes\PluginBase;

/**
 * School Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'School',
            'description' => 'No description provided yet...',
            'author'      => 'Smartschool',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Smartschool\School\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'smartschool.school.some_permission' => [
                'tab' => 'School',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'school' => [
                'label'       => 'School',
                'url'         => Backend::url('smartschool/school/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['smartschool.school.*'],
                'order'       => 500,
            ],
        ];
    }
}
