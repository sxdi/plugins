<?php namespace Smartschool\Academy\Models;

use Model;
use Smartschool\Core\Classes\Generator;

/**
 * Academy Model
 */
class Academy extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_academy_academies';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }
}
