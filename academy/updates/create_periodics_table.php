<?php namespace Smartschool\Academy\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePeriodicsTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_academy_periodics', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('academy_id');
            $table->date('date');
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_academy_periodics');
    }
}
