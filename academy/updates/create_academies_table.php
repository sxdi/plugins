<?php namespace Smartschool\Academy\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAcademiesTable extends Migration
{
    public function up()
    {
        Schema::create('smartschool_academy_academies', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('school_id');
            $table->string('name');
            $table->date('started_at');
            $table->date('ended_at');
            $table->boolean('is_active')->nullable()->default(0);
            $table->string('parameter');
        });
    }

    public function down()
    {
        Schema::dropIfExists('smartschool_academy_academies');
    }
}
