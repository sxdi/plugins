<?php namespace Smartschool\Teacher\Models;

use Model;
use Smartschool\Core\Classes\Generator;
use Smartschool\Core\Classes\UserManager;
use Smartschool\Core\Classes\SchoolManager;

/**
 * Teacher Model
 */
class Teacher extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'smartschool_teacher_teachers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'user' => [
            'Smartschool\User\Models\User',
            'key'      => 'user_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function getUser()
    {
        $user   = new UserManager;
        return $user->getUser();
    }

    public function getSchool()
    {
        $user   = $this->getUser();
        $school = new SchoolManager;
        return $school->get($user->id);
    }

    public function getAllBySchool()
    {
        $school = $this->getSchool();
        return $this->whereSchoolId($school)->get();
    }

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->school_id = $this->getSchool();
        $this->parameter = $generator->make();
    }
}
