<?php namespace Smartschool\Teacher;

use Backend;
use System\Classes\PluginBase;

/**
 * Teacher Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Teacher',
            'description' => 'No description provided yet...',
            'author'      => 'Smartschool',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Smartschool\Teacher\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'smartschool.teacher.some_permission' => [
                'tab' => 'Teacher',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'teacher' => [
                'label'       => 'Teacher',
                'url'         => Backend::url('smartschool/teacher/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['smartschool.teacher.*'],
                'order'       => 500,
            ],
        ];
    }
}
